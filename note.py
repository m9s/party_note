#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields

class Note(ModelSQL, ModelView):
    "Note"
    _description = __doc__
    _name = "note.note"

    name = fields.Char('Name')
    description = fields.Text('Description')
    party = fields.Many2One('party.party', 'Party', select=1,
            ondelete='CASCADE')

Note()
