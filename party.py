#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields

class Party(ModelSQL, ModelView):
    _name = "party.party"

    notes = fields.One2Many('note.note', 'party', 'Notes')

Party()